#pragma once

#include <iostream>
#include <string>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>
#include <cryptopp/files.h>

using namespace CryptoPP;

class HashObject {
    private:
        std::string hash = "";

        std::string str_to_SHA1(std::string);

    public:
        std::string get();
        void set(std::string);
};