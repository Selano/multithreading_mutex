#pragma once

#include "hash_object.hpp"
#include <string>
#include <mutex>

class ContainerObject {
    private:
        HashObject hash;
        std::mutex containMute;

    public:
        void set_hash(std::string);
        std::string read_hash();
};