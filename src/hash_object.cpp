#include "../include/hash_object.hpp"

void HashObject::set(std::string msg) {
    this->hash = str_to_SHA1(msg);
}

std::string HashObject::str_to_SHA1(std::string source) {
    CryptoPP::SHA1 hash;
    byte digest[CryptoPP::SHA1::DIGESTSIZE];
    hash.CalculateDigest(digest, (const byte*)source.c_str(), source.size());
    std::string output;
    CryptoPP::HexEncoder encoder;
    CryptoPP::StringSink test = CryptoPP::StringSink(output);
    encoder.Attach(new CryptoPP::StringSink(output));
    encoder.Put(digest, sizeof(digest));
    encoder.MessageEnd();
    return output;
}

std::string HashObject::get() {
    return hash;
}