#include "../include/container_object.hpp"

void ContainerObject::set_hash(std::string msg) {
    std::unique_lock<std::mutex> guard(this->containMute, std::defer_lock);

    guard.lock();

    this->hash.set(msg);

    guard.unlock();
}

std::string ContainerObject::read_hash() {
    std::lock_guard<std::mutex> guard(this->containMute);
    
    return this->hash.get();
}