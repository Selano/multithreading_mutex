#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>
#include <queue>
#include <functional>
#include "../include/container_object.hpp"

void requests_hash(ContainerObject *container, std::string msg) {
    static std::mutex mute;

    for(int i = 0; i < 10; i++) {
        std::this_thread::sleep_for(std::chrono::microseconds(20));
        container->set_hash(msg);
        mute.lock();
        std::cout << "Thread " << std::this_thread::get_id() << " : " \
        << container->read_hash() << std::endl;
        mute.unlock();
    }
}

int main() {
    ContainerObject *container = new ContainerObject;

    // std::vector<std::thread> threadRequests;
    std::queue<std::thread> threadRequests;

    threadRequests.push(std::thread(std::bind(requests_hash, container, "Hello, Cryptopp!")));
    threadRequests.push(std::thread(std::bind(requests_hash, container, "Hello, Thread!")));
    threadRequests.push(std::thread(std::bind(requests_hash, container, "Hello, Mutex!")));
    threadRequests.push(std::thread(std::bind(requests_hash, container, "Hello, Chromo!")));
    threadRequests.push(std::thread(std::bind(requests_hash, container, "Hello, Functional!")));

    while (!threadRequests.empty())
    {
        (threadRequests.front()).join();
        threadRequests.pop();
    }
    
    delete container;
}